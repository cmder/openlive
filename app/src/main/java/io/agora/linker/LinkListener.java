package io.agora.linker;

/**
 * Created by wubingshuai on 2017/9/14.
 */

public interface LinkListener {
    void onConfirmClicked(int uid) ;
    void onCancelClicked(int uid) ;
    void onListEmptyNotify() ;
}
