package io.agora.account;

import io.agora.utils.LinkState;

/**
 * Created by wubingshuai on 2017/8/24.
 */

public interface IAccountClickListener {
    void onAccountClick(int id , boolean isChecked , int mediaType) ;
    void onLinkOrTClicked(int uid , LinkState currentState) ;
}
